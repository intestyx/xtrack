import { IAppState } from '../reducers';

export function getAppPosts(state: IAppState): object[] {
    return state.posts;
}

export function getAppPostsIsProcessing(state: IAppState): boolean {
    return state.postsIsProcessing;
}